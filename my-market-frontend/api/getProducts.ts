import { productSchema } from "@/types";
import qs from "qs";

const productsSchema = productSchema.array();

const query = () => {
  return qs.stringify(
    {
      populate: {
        name: true,
        quantity: true,
        price: true,
        image: { populate: "*" },
      },
    },
    {
      encodeValuesOnly: true, // prettify URL
    }
  );
};

export const getProducts = async () => {
  try {
    const result = await fetch(
      `${process.env.STRAPI_ENDPOINT}/api/products?${query()}`
    );
    if (!result.ok) throw new Error("Failed to fetch data");
    const resData = await result.json();

    if (resData?.data.length === 0) return null;

    const formattedProducts = resData.data.map((product: any) => ({
      name: product.attributes.name,
      quantity: product.attributes.quantity,
      price: product.attributes.price,
      image: {
        url:
          process.env.STRAPI_ENDPOINT +
          product.attributes.image.data.attributes.url,
        alt: product.attributes.image.data.attributes.name,
        width: product.attributes.image.data.attributes.width,
        height: product.attributes.image.data.attributes.height,
      },
    }));

    const productsData = productsSchema.safeParse(formattedProducts);
    if (!productsData.success) return null;

    return productsData.data;
  } catch (err) {
    console.error("error 💥", err);
  }
};
