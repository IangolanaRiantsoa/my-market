"use client";
import ProductCard from "@/components/ProductCard"
import useLocalStorage from "../utils/localStorageHook"
import { ProductType } from "@/types"

const ProductsClient = () => {
  const productsFromLocalStorage = useLocalStorage('products')
  const products = productsFromLocalStorage !== null ? JSON.parse(productsFromLocalStorage) : []
  return (
    products.length !== 0 &&
      products.map((product: ProductType, index: number) => (
        <ProductCard {...{ ...product }} key={product.name + index} />
      ))
  )
};

export default ProductsClient;