import { getProducts } from "@/api/getProducts";
import ProductCard from "@/components/ProductCard";
import { ProductType } from "@/types";
import ProductsClient from "./ProductsClient";

export default async function Products() {
  const products: ProductType[] | null | undefined = await getProducts();

  return (
    <main className="flex min-h-screen justify-center">
      <section className="flex flex-col items-center justify-between p-24 space-y-4 w-full lg:flex-row lg:flex-wrap max-w-5xl">
        <ProductsClient/>
      </section>
    </main>
  );
}
