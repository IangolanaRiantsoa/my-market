"use client";

import ShoppingCard from "@/components/ShoppingCard";
import useLocalStorage from "../utils/localStorageHook";
import { BasketProductType } from "@/types";
import Image from 'next/image';
import emptyIcon from '@/public/images/empty-cart.svg';
import Link from "next/link";

const CartClient = () => {
  const productsFromStorage = useLocalStorage('allProductsInBasket')
  const productInCart: BasketProductType[] = !productsFromStorage ? [] : JSON.parse(productsFromStorage);

  const allProductsInBasketFromStorage = useLocalStorage("allProductsInBasket");

  const totalProductsPrice = () => {
    if (allProductsInBasketFromStorage === null) return 0;
    const productsData:BasketProductType[] = JSON.parse(allProductsInBasketFromStorage);
    if (productsData.length === 0) return 0;

    return productsData.reduce((acc:number, it:BasketProductType) => acc + (it.product.price * it.quantityBooked),0).toFixed(2)
  }

  const handlecloseModal = () => {
    const modal = document.getElementById('my_modal') as HTMLDialogElement | null;
    if (modal) modal.showModal();
  }

  if (productInCart.length === 0)
    return(
      <section className="spy-5">
        <Image src={emptyIcon} alt="empty cart" width={500} height={500} className="w-52"/>

        <p>Votre panier est encore vide.</p>
        <Link href="/" className="text-blue-500 underline underline-offset-2 text-center block">Voir les produits</Link>
      </section>
    )

  return(
    <>
      <div className="space-y-4">
        {
          productInCart.length !== 0 && productInCart.map((p: BasketProductType, index: number) =>
          (
            <ShoppingCard
              productItem={p}
              key={p.product.name+' '+index}
            />
          ))
        }
      </div>

      <div className="card w-96 bg-primary-ivory my-5">
        <div className="card-body">
          <p className="font-bold text-primary-dark-cyan">Total: </p>
          <p className="text-2xl font-semibold">{totalProductsPrice()}€</p>
          <div className="card-actions justify-end">
            <button 
              className="btn btn-primary text-white"
              onClick={handlecloseModal}
            >
              Proceder au payement
            </button>
          </div>
        </div>
      </div>

      <dialog id="my_modal" className="modal">
        <div className="modal-box">
          <h3 className="font-bold text-lg text-center">My Market</h3>
          
          <div className="space-y-4 flex items-center flex-col w-[464px] my-5">
            <label className="input input-bordered flex items-center gap-2 w-80">
              <input type="text" className="grow" placeholder="Email" />
            </label>

            <label className="input input-bordered flex items-center gap-2 w-80">
              <input type="number" className="grow" placeholder="Numero de carte" />
            </label>

            <div className="flex justify-between space-x-6">
              <label className="input input-bordered flex items-center gap-2">
                <input type="text" className="grow w-28" placeholder="MM/YY" />
              </label>

              <label className="input input-bordered flex items-center gap-2">
                <input type="number" className="grow w-28" placeholder="CVC" />
              </label>
            </div>

            <button 
              className="btn btn-primary text-white"
            >
              Payer {totalProductsPrice()}€
            </button>
          </div>
        </div>
        <form method="dialog" className="modal-backdrop">
          <button>close</button>
        </form>
      </dialog>
    </>
  );
}

export default CartClient;