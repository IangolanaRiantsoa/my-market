import CartClient from "./CartClient";

const Cart = () => {
  return (
    <main className="flex flex-col min-h-screen items-center mt-24">
      <h1 className="text-4xl text-primary-dark-cyan font-semibold my-5">
        Shopping cart
      </h1>

      <CartClient/>
    </main>
  );
};

export default Cart;
