import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import Menu from "@/components/menu";
import clsx from "clsx";
import { getProducts } from "@/api/getProducts";
import { ProductType } from "@/types";
import InitData from "@/components/initData";
import { Suspense } from "react";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "My market",
  description: "Buy with confidence",
};

export default async function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  const products: ProductType[] | null | undefined = await getProducts();
  return (
    <html lang="en">
      <body className={clsx(inter.className, "py-5")}>
        <Menu/>
        {children}
        <Suspense>
          <InitData products={products}/>
        </Suspense>
      </body>
    </html>
  );
}
