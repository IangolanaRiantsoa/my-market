"use client";
import Image from "next/image";
import addToCardIcon from "@/public/images/add-to-cart.svg";
import { useState } from "react";
import { BasketProductType, ProductType } from "@/types";
import useLocalStorage, { setLocalStorage } from "@/app/utils/localStorageHook";
import useProductLogique from "@/app/utils/productLogic";

const Card = ({ image, name, price, quantity }: ProductType) => {
  const {
    stockQuantity,
    productInBasket,
    handleAddProductInBasket,
    handleRemoveProductInBasket
  } = useProductLogique({ image, name, price, quantity })

  return (
    <div className="card w-80 bg-base-100 shadow-xl">
      <figure>
        <Image
          src={image.url}
          alt={image.alt}
          width={image.width}
          height={image.height}
          className="w-full h-48"
        />
      </figure>
      <div className="card-body">
        <h2 className="card-title">{name}</h2>
        <div>
          <span className="font-semibold">Quantité en stock: </span>
          <span>{stockQuantity}</span>
        </div>

        <div className="card-actions justify-between items-center">
          <p className="text-2xl font-semibold text-primary-bittersweet">
            {price}€
          </p>

          {productInBasket === 0 ? (
            <button
              className="btn btn-primary"
              onClick={handleAddProductInBasket}
            >
              <Image
                src={addToCardIcon}
                alt="add to card"
                width={30}
                height={30}
              />
            </button>
          ) : (
            <div className="flex justify-between items-center space-x-1">
              <button
                className="btn btn-circle text-xl"
                onClick={handleRemoveProductInBasket}
              >
                -
              </button>
              
              <kbd className="kbd">{productInBasket}</kbd>
              
              <button
                className="btn btn-circle text-xl"
                onClick={handleAddProductInBasket}
              >
                +
              </button>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default Card;
