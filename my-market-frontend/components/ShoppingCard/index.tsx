import Image from "next/image";
import trashIcon from "@/public/images/trash-icon.svg";
import { BasketProductType } from "@/types";
import useProductLogique from "@/app/utils/productLogic";

interface IShoppingCard {
  productItem: BasketProductType;
}

const ShoppingCard = ({productItem}: IShoppingCard) => {
  const {
    handleAddProductInBasket,
    handleRemoveProductInBasket,
    handleRemoveInCart
  } = useProductLogique(productItem.product);

  return (
    <div className="card md:card-side bg-base-100 shadow-xl">
      <figure className="w-80 md:w-96 h-auto">
        <Image
          src={productItem.product.image.url}
          width={productItem.product.image.width}
          height={productItem.product.image.height}
          alt={productItem.product.image.alt}
          className="w-full md:h-full"
        />
      </figure>
      <div className="card-body">
        <div className="space-x-4">
          <span>Nom du produit:</span>
          <span className="font-bold">{productItem.product.name}</span>
        </div>

        <div className="space-x-4">
          <span>Quantité en stock:</span>
          <span className="font-bold">{productItem.product.quantity}</span>
        </div>

        <div className="space-x-4">
          <span>Prix:</span>
          <span className="font-bold">{productItem.product.price}€</span>
        </div>

        <div>
          <p className="font-bold">Dans le panier:</p>

          <div className="flex justify-between items-center">
            <button 
              className="btn btn-circle text-xl" 
              onClick={handleRemoveProductInBasket}
            >
              -
            </button>
            
            <kbd className="kbd">{productItem.quantityBooked}</kbd>
            
            <button 
              className="btn btn-circle text-xl" 
              onClick={handleAddProductInBasket}
            >
              +
            </button>
          </div>
        </div>

        <div className="space-x-4">
          <span className="font-bold">A payer:</span>
          <kbd className="kbd">{(productItem.product.price * productItem.quantityBooked).toFixed(2)}€</kbd>
        </div>

        <div className="card-actions justify-center md:justify-end my-4">
          <button className="btn btn-error" onClick={handleRemoveInCart}>
            <Image src={trashIcon} width={30} height={30} alt="trash" />
            Enlever
          </button>
        </div>
      </div>
    </div>
  );
};

export default ShoppingCard;
