"use client";
import { setLocalStorage } from "@/app/utils/localStorageHook";
import { ProductType } from "@/types"
import { useEffect } from "react";

type InitDataType = {
  products: ProductType[] | null | undefined;
}


const InitData = ({products}:InitDataType ) => {
  useEffect(() => {
    setLocalStorage(
      "products",
      JSON.stringify(products)
    );
  }, []);

  return <></>
}

export default InitData;