"use client";
import Image from "next/image";
import menuIcon from "@/public/images/menu.svg";
import marketIcon from "@/public/images/market-stall.svg";
import shoppingCart from "@/public/images/shopping-cart.svg";
import Link from "next/link";
import { useEffect, useState } from "react";
import { BasketProductType } from "@/types";
import useLocalStorage from "@/app/utils/localStorageHook";

const Menu = () => {
  const [productsInBasketNumber, setProductsInBasketNumber] = useState(0);
  const allProductsInBasket = useLocalStorage("allProductsInBasket");

  useEffect(() => {
    const allProducts = allProductsInBasket ? JSON.parse(allProductsInBasket): [];
    const totalQuantityBooked = allProducts.reduce(
      (acc: number, item: BasketProductType) => acc + item.quantityBooked,
      0
    );
    setProductsInBasketNumber(totalQuantityBooked);
  }, [allProductsInBasket]);

  return (
    <section className="fixed z-50 px-5 w-full flex justify-center">
      <div className="bg-base-200 shadow-xl w-full h-16 rounded-3xl flex px-4 justify-between items-center max-w-5xl">
        <details className="dropdown">
          <summary className="m-1 btn">
            <Image src={menuIcon} alt="menu-icon" height={30} width={30} />
          </summary>
          <ul className="p-2 shadow menu dropdown-content z-[1] bg-base-100 rounded-box w-52">
            <li>
              <Link href="/">Accueil</Link>
            </li>
            <li>
              <Link href="/cart">Mon panier</Link>
            </li>
          </ul>
        </details>

        <div className="flex items-center">
          <Image src={marketIcon} alt="menu-icon" height={30} width={30} />
          <span className="text-2xl">
            <Link href="/">My Market</Link>
          </span>
        </div>

        <div className="relative">
          <div className="absolute bg-primary-bittersweet text-white rounded-full h-6 w-6 bottom-4 left-3">
            <p className="text-white text-center font-bold">
              {productsInBasketNumber}
            </p>
          </div>

          <Link href="/cart">
            <Image src={shoppingCart} alt="menu-icon" height={30} width={30} />
          </Link>
        </div>
      </div>
    </section>
  );
};

export default Menu;
