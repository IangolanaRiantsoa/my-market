import { z } from "zod";

export const productSchema = z.object({
  name: z.string(),
  quantity: z.number(),
  price: z.number(),
  image: z.object({
    url: z.string(),
    alt: z.string(),
    width: z.number(),
    height: z.number(),
  }),
});

export type ProductType = z.infer<typeof productSchema>;

export type BasketProductType = {
  product: ProductType;
  quantityBooked: number;
};
