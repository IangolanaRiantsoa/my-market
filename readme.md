# My Market (react/vue kata)

## Consignes
Les instructions suivantes vous permettront d'obtenir une copie du projet opérationnel en local à des fins de développement et de test.

## Fonctionnalités

> - Afficher la liste des produits disponibles(avec stock disponible, prix...).
> - Ajouter, modifier ou retirer des produits dans un panier.
> - Retirez des prodtuits du panier si la date dépasse du 24h

### Technologies utilisées:
 - front-end: reactjs (nextjs), tailwindcss
 - back-end: strapi
 - git

## Configuration
>- Cloner le projet en ssh ou http
>- Entrer dans le dossier racine de l'application `my-market`
```
cd `my-market
```

### Back-end
>- Créer un fichier .env et pour ce test mettre les infos suivantes même si c'est contre intuitif dans un vrai projet
  HOST=0.0.0.0
  PORT=1337
  APP_KEYS=n/TbH1UcJmtsGf0Ct/FUDA==,gMDI1FhYfVqN984F0+PUSw==,00MABuIZAVdS5fq3lpBvaw==,9tABCQoQ5erjUIyusJzPaA==
  API_TOKEN_SALT=ooxzFxzTR/4bOEoN02XZsg==
  ADMIN_JWT_SECRET=hNeLg0A8Gf9Y4blcRKsDBw==
  TRANSFER_TOKEN_SALT=/Jx87nlnfWZQvkMkUfKfEA==
  # Database
  DATABASE_CLIENT=sqlite
  DATABASE_FILENAME=.tmp/data.db
  JWT_SECRET=Ajt7kuR0XwLiDWVNQSuyIw==

>- Entrer dans le dossier contenant le strapi `my-market/my-market-backend`
>- Installer les packages en executant 

```
cd my-market-backend
npm i
```
>- Lancer le backend avec la commande

```
npm run strapi develop
```
>- Ouvrir `http://localhost:1337/` sur le navigateur et suivre les instructions

### Front-end
>- Entrer dans le dossier contenant le strapi `my-market/my-market-frontend`

>- Créer un fichier `.env` et pour ce test mettre l'info suivante même si c'est contre intuitif dans un vrai projet
STRAPI_ENDPOINT=http://localhost:1337

>- Installer les packages en executant 
```
npm i
```

>- Lancer le projet
```
npm run dev
```

>- Ouvrir le navigateur sur `http://localhost:3000/`

## Auteur

**Iangolana Riantsoa Ramelson** - [riantsoaramelson@gmail.com](mailto:riantsoaramelson@gmail.com)